<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Ingredients;

class DefaultController extends AbstractController
{
    /**
     * @Route("/hello/{name}", name="default")
     */
    public function hello($name)
    {
        return new Response('<html><head></head><body>hello '.$name.' !</body></html>');
    }

    /**
     * @Route("/ingredient/list", name="ingredient_list")
     */
    public function ingredient_list()
    {  
        $em = $this->getDoctrine()->getManager();
        $ingredientsRepository = $em->getRepository(Ingredients::class);
        $ingredients = $ingredientsRepository->findAll();
        dump($ingredients);
        die;
    }

    /**
     * @Route("/ingredient/create/{nom}/{valeurNutrition}", name="ingredient_create")
     */
    public function ingredient_create($nom, $valeurNutrition)
    {
        $em = $this->getDoctrine()->getManager();
        $ingredientsRepository = $em->getRepository(Ingredients::class);

        $ingredient = new Ingredients();
        $ingredient->setNom($nom);
        $ingredient->setValeurNutritionnelle($valeurNutrition);

        $em->persist($ingredient);
        $em->flush();
    }

    /**
     * @Route("/echo", name="echo")
     */
    public function echo_get(Request $request)
    {
        $searchQuery = $request->query;
        
        dump($searchQuery);
        die;
    }
}
