<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200210102603 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE recette (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, duree INT NOT NULL, difficulte INT NOT NULL, portions INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recette_ustenciles (recette_id INT NOT NULL, ustenciles_id INT NOT NULL, INDEX IDX_8D364A5589312FE9 (recette_id), INDEX IDX_8D364A5598CBE440 (ustenciles_id), PRIMARY KEY(recette_id, ustenciles_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ustenciles (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE recette_ustenciles ADD CONSTRAINT FK_8D364A5589312FE9 FOREIGN KEY (recette_id) REFERENCES recette (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recette_ustenciles ADD CONSTRAINT FK_8D364A5598CBE440 FOREIGN KEY (ustenciles_id) REFERENCES ustenciles (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE recette_ustenciles DROP FOREIGN KEY FK_8D364A5589312FE9');
        $this->addSql('ALTER TABLE recette_ustenciles DROP FOREIGN KEY FK_8D364A5598CBE440');
        $this->addSql('DROP TABLE recette');
        $this->addSql('DROP TABLE recette_ustenciles');
        $this->addSql('DROP TABLE ustenciles');
    }
}
